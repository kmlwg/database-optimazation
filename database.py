from typing import List

import cx_Oracle
import data_models as model



def connect(user:str, pswd:str, dsn:str, encoding:str)->cx_Oracle.Connection:
    connection = cx_Oracle.connect(user=user, password=pswd, dsn=dsn, encoding=encoding)

    return connection

    
def insert_languages(connection:cx_Oracle.Connection, langs:List[str])->None:
    """ 
    Inserts languages to the database
        langs : {language : iso_code}
    """
    query = 'INSERT INTO LANGUAGES(NAME, ISO_CODE) VALUES(:NAME, :CODE)'

    try:
        with connection as connection:
            with connection.cursor() as cursor:
                for name, code in langs.items():
                    cursor.execute(query, [name, code])
                    connection.commit()
    except cx_Oracle.Error as error:
        print('ORACLE ERROR:')
        print(error)


def insert_countries(connection:cx_Oracle.Connection, countries:List[str])->None:       
    """ 
    Inserts countries to the database
        countries : {country : iso_code}
    """
    query = 'INSERT INTO COUNTRIES(NAME, ISO_CODE) VALUES(:NAME, :CODE)'

    try:
        with connection as connection:
            with connection.cursor() as cursor:
                for name, code in countries.items():
                    cursor.execute(query, [name, code])
                    connection.commit()
    except cx_Oracle.Error as error:
        print('ORACLE ERROR:')
        print(error)

    
def insert_certs(connection:cx_Oracle.Connection, certs:List[str])->None:
    """ 
    Inserts certs (ie. PG) to the database
        certs : [] 
    """
    query = 'INSERT INTO CERTS(NAME) VALUES(:NAME)'

    try:
        with connection as connection:
            with connection.cursor() as cursor:
                for c in certs:
                    cursor.execute(query, [c])
                    connection.commit()
    except cx_Oracle.Error as error:
        print('ORACLE ERROR:')
        print(error)


def insert_movies(connection:cx_Oracle.Connection, movies:List[model.Movie])->None:
    query = ( 
        "INSERT INTO MOVIES("
        "TITLE, PREMIERE, DURATION, RATING, NUMBER_RATINGS, METASCORE, URL) "
        "VALUES(:TITLE, TO_DATE(:PREMIERE, 'yyyy/mm/dd'), :DURATION, :RATING, :NUMBER_RATINGS, :METASCORE, :URL)"
    )

    try:
        with connection:
            with connection.cursor() as cursor:
                for m in movies:
                    date = m.year + "/01/01"
                    cursor.execute(query, [m.title, date, int(m.duration), float(m.rating), int(m.votes), int(m.metascore), m.url])
                    connection.commit()
    except cx_Oracle.Error as error:
        print('ORACLE ERROR:')
        print(error)

    
def insert_genres(connection:cx_Oracle.Connection, genres:List[str])->None:
    query = "INSERT INTO GENRES(NAME) VALUES(:NAME)"

    try:
        with connection:
            with connection.cursor() as cursor:
                for g in genres:
                    cursor.execute(query, [g])
                    connection.commit()
    except cx_Oracle.Error as error:
        print(error)


def update_movie_full(connection:cx_Oracle.Connection, id:int, movie:model.MovieExtra)->None:
    query = ('UPDATE MOVIES '
             'SET '
                'BUDGET=:BUDGET,'
                'BOXOFFICE=:BOXOFFICE,'
                'STORYLINE=:STORYLINE '
             'WHERE ID=:ID')
    args = [movie.budget, movie.box_office, movie.desc, id]
    cur = connection.cursor()
    cur.execute(query, args)
    connection.commit()    


def insert_person_simple(connection:cx_Oracle.Connection, name:str, url:str)->None:
    query = ('INSERT INTO PERSONS(NAME,URL) '
                'VALUES(:NAME,:URL)'
    )
    args = [name, url]
    cur = connection.cursor()
    try:
        cur.execute(query, args)
        connection.commit()       
    except cx_Oracle.IntegrityError as e:
        print(e)
    # if cur.rowcount() == 0:
    #     query = 'INSERT INTO PERSONS(NAME,URL) VALUES(:NAME,:URL)'
    #     cur.execute(query, [name, url])
    #     connection.commit()       
    

def insert_job(connection:cx_Oracle.Connection, movie_id:int,  job_id:str, person_id:int)->None:
    query = ('INSERT ALL '
                'INTO MOVIES_JOBS(MOVIE_ID,JOB_ID)'
                    'VALUES(:MOVIE_ID,:JOB_ID)'
                'INTO PERSONS_JOBS(PERSON_ID, JOB_ID) VALUES(:PERSON_ID,:JOB_ID) '
                'SELECT * FROM DUAL'
    )
    args = [movie_id, job_id, person_id, job_id]
    cur = connection.cursor()
    cur.execute(query, args)
    connection.commit()


def insert_character(connection:cx_Oracle.Connection, name:str,movie_id:int, person_id:int)->None:
    query = ('SELECT NULL FROM ROLES '
             'WHERE '
                'NAME=:NAME AND '
                'MOVIE_ID=:MOVIE_ID AND '
                'PERSON_ID=:PERSON_ID')
    cur = connection.cursor()
    args = [name, movie_id, person_id]
    cur.execute(query, args)
    if cur.rowcount == 0:
        query = ('INSERT INTO ROLES(NAME,MOVIE_ID,PERSON_ID)'
                'VALUES(:NAME, :MOVIE_ID, :PERSON_ID) '
        )
        args = [name, movie_id, person_id]
        cur.execute(query, args)
        connection.commit()

    return None


def get_movie_url(connection:cx_Oracle.Connection, id: int)->str:
    query = 'SELECT URL FROM MOVIES WHERE MOVIES.ID = :ID'
    cur = connection.cursor()
    cur.execute(query, [id])
    url = cur.fetchone()
    if url is None:
        return ''
    else:
        return url[0]

def insert_person(connection:cx_Oracle.Connection, name:str, url:str)->None:
    query = ('INSERT INTO PERSONS(NAME,URL)'
                'VALUES(:NAME,:URL)'
             'WHERE NOT EXISTS('
                'SELECT NULL FROM PERSONS WHERE URL=:URL)'
    )
    args = [name, url, url]
    cur = connection.cursor()
    cur.execute(query, args)
    connection.commit()

    return None


def get_job_id(connection:cx_Oracle.Connection, name:str)->int:
    query = 'SELECT ID FROM JOBS WHERE NAME=:NAME'
    cur = connection.cursor()
    cur.execute(query, [name])
    connection.commit()
    
    return int(cur.fetchone()[0])

def get_genre_id(connection:cx_Oracle.Connection, name:str)->int:
    query = 'SELECT ID FROM GENRES WHERE NAME=:NAME'
    cur = connection.cursor()
    cur.execute(query, [name])
    connection.commit()
    
    return int(cur.fetchone()[0])


def insert_movies_genres(connection:cx_Oracle.Connection, movie_id, genre_id)->None:
    query = 'INSERT INTO MOVIES_GENRES(MOVIE_ID,GENRE_ID) VALUES(:MOVIE_ID,:GENRE_ID)'
    cur = connection.cursor()
    cur.execute(query, [movie_id, genre_id])
    connection.commit()

    return None


def get_lang_id(connection:cx_Oracle.Connection, name:str)->int:
    query = 'SELECT ID FROM LANGUAGES WHERE NAME=:NAME'
    cur = connection.cursor()
    cur.execute(query, [name])
    connection.commit()
    
    return int(cur.fetchone()[0])


def insert_movies_langs(connection:cx_Oracle.Connection, movie_id, lang_id):
    query = 'INSERT INTO MOVIES_LANGUAGES(MOVIE_ID,LANGUAGE_ID) VALUES(:MOVIE_ID,:LANG_ID)'
    cur = connection.cursor()
    cur.execute(query, [movie_id, lang_id])
    connection.commit()

    return None

def get_country_id(connection:cx_Oracle.Connection, name:str)->int:
    query = 'SELECT ID FROM COUNTRIES WHERE NAME=:NAME'
    cur = connection.cursor()
    cur.execute(query, [name])
    connection.commit()
    
    return int(cur.fetchone()[0])

def insert_movies_countries(connection:cx_Oracle.Connection, movie_id, country_id):
    query = 'INSERT INTO MOVIES_COUNTRIES(MOVIE_ID,COUNTRY_ID) VALUES(:MOVIE_ID,:COUNTRY_ID)'
    cur = connection.cursor()
    cur.execute(query, [movie_id, country_id])
    connection.commit()

    return None


def get_person_id_by_url(connection:cx_Oracle.Connection, url:str)->int:
    query = ('SELECT ID FROM PERSONS WHERE URL=:URL')
    args = [url]
    cur = connection.cursor()
    cur.execute(query, args)
    connection.commit()

    id = int(cur.fetchone()[0])

    return id