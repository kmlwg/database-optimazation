import scrap
import database as db
import config as cfg
from time import sleep
import random


if __name__=='__main__':
    connection = db.connect(cfg.user, cfg.password, cfg.dsn, cfg.encoding)

    # langs = scrap.scrap_languages()
    # db.insert_languages(connection, langs)
    # del langs

    # countries = scrap.scrap_countries()
    # db.insert_countries(connection, countries)
    # del countries

    # for year in range(1970,2020):
    #     for n in range(301,502,50):
    #         movies = scrap.scrap_movie_search(n, year)
    #         print('Scrapped year {} page {}'.format(year,n))
    #         db.insert_movies(connection, movies)
    #         print('Saved year {} page {}'.format(year,n))
    #         connection = db.connect(cfg.user, cfg.password, cfg.dsn, cfg.encoding)

#    genres = scrap.scrap_genres('./data/genres')
#    db.insert_genres(connection, genres)

    # certs = scrap.scrap_certs()
    # db.insert_certs(connection, certs)

    # l = scrap.scrap_movie_page('https://www.imdb.com/title/tt0172495')
    # print(l)

    ids = [2]
    for id in ids:
        url = db.get_movie_url(connection, id)
        movie_details, genres, writers, directors, cast = scrap.scrap_movie_page(url)
        db.update_movie_full(connection, id, movie_details)
        for p in cast:
            db.insert_person_simple(connection, p[0], p[1])
        for name, url in directors.items():
            db.insert_person_simple(connection, name, url)
        for name, url in writers.items():
            db.insert_person_simple(connection, name, url)
        for p in cast:
            person_id = db.get_person_id_by_url(connection, p[1])
            db.insert_character(connection, p[2], id, person_id)
        job_id = db.get_job_id(connection, 'writer')
        for url in writers.keys():
            person_id = db.get_person_id_by_url(connection, url)
            db.insert_job(connection, id, job_id, person_id)
        job_id = db.get_job_id(connection,'director')
        for url in directors.keys():
            person_id = db.get_person_id_by_url(connection, url)
            db.insert_job(connection, id, job_id, person_id)
        for g in genres:
            genre_id = db.get_genre_id(connection, g)
            db.insert_movies_genres(connection, id, genre_id)
        for l in movie_details.languages:
            lang_id = db.get_lang_id(connection, l)
            db.insert_movies_langs(connection, id, lang_id)
        for c in movie_details.countries:
            if c == 'USA':
                c = 'United States'
            country_id = db.get_country_id(connection, c)
            db.insert_movies_countries(connection, id, country_id)
    connection.close()
    
    

