from typing import List



class Movie():
    def __init__(self, title, year, duration, rating, votes, metascore, desc,url):
        self.title = title
        self.year = year
        self.duration = duration
        self.rating = rating
        self.votes = votes
        self.metascore = metascore
        self.desc = desc
        self.url = url


class MovieExtra():
    def __init__(self, budget:int, box_office:int, countries:List[str], languages:List[str], desc:str):
        self.budget = budget
        self.box_office = box_office
        self.countries = countries
        self.languages = languages
        self.desc = desc