from requests import get
from typing import List, Dict
from bs4 import BeautifulSoup

import imdb_urls as urls
import data_models as models



def scrap_page(url):
    res = get(url)
    return BeautifulSoup(res.text, 'html.parser')


def scrap_languages():
    """ 
    Scraps search page for languages.
    Returns:
        {language : iso_code}
    """
    html_soup = scrap_page(urls.search_page)
    langs_container = html_soup.find('select', class_='languages')
    langs_names = [l for l in langs_container.stripped_strings]
    iso_codes = [o.get('value') for o in langs_container.find_all('option')]

    langs = dict(zip(langs_names, iso_codes))

    return langs


def scrap_countries():
    """
    Scraps search page for countries.
    Returns:
        {country : iso_code}
    """
    html_soup = scrap_page(urls.search_page)
    countries_container = html_soup.find('select', class_='countries')
    countries_names = [c for c in countries_container.stripped_strings]
    iso_codes = [o.get('value') for o in countries_container.find_all('option')]

    countries = dict(zip(countries_names, iso_codes))

    return countries

    
def search_string_builder(year: int, start: int):
    url = urls.movie_search + str(year) + '-01-01,' + str(year) + '-12-31&start=' + str(start)
    return url


def scrap_movie_search(n: int, year: int)->List:
    """
    Scrap movie search for basic information about movies
    Arguments:
        n_peryear: int - number of movies to be scrapped per year
        yea:       int - realese date of movies to be scraooed
    Returns:
        [Movie]
    """
    movies = []
    url = search_string_builder(year, n)
    print(url)
    soup = scrap_page(url)
    movies_containers = soup.find_all('div', class_='lister-item mode-advanced')
    print('start')
    for m in movies_containers:
        if m.find('div', class_='ratings-metascore') is not None:
            title = m.h3.a.text
            m_url = m.h3.a['href']
            m_url = 'https://www.imdb.com' + m_url
            year  = m.find('span', class_='lister-item-year').text
            # Get rid of '(' and ')'
            year = year[1:-1]
            duration  = m.find('span', class_='runtime').text
            duration = duration[0:-4]
            rating    = float(m.strong.text)
            votes     = m.find('span', attrs={'name':'nv'})['data-value']
            desc      = m.find('p', class_='text-muted').text
            metascore = m.find('span', class_ = 'metascore').text
            movie = models.Movie(title, year, duration, rating, votes,metascore, desc, m_url)
            movies.append(movie)
                
    return movies

def scrap_genres(path:str)->List[str]:
    with open(path) as f:
        data = f.read()
        genres = data.split(',')
        return genres

def scrap_certs()->List[str]:
    certs = ['NA', 'G', 'PG', 'PG-13', 'R']
    return certs


def _clean_budget_string(budget:str)->str:
    budget = budget.replace('$','')
    budget = budget.replace(',','')
    budget = budget.replace('\n','')
    budget = budget[7::].split(' ')[0]
    return budget


def _clean_box_office_string(box_office:str)->str:
    box_office = box_office.replace('$','')
    box_office = box_office.replace(',','')
    box_office = box_office.split(':')[1]
    box_office = int(box_office.split(' ')[1])
    return box_office


def _clean_summary_string(summary: str)->str:
    summary = summary.replace('\n','')
    summary = summary.split('.')[0] + '.'
    
    l = 0
    for s in summary.split(' '):
        if s:
            break
        l = l + 1
    summary = summary[l::]
    return summary


def _scrap_movie_extra_parameters(soup:BeautifulSoup)->models.MovieExtra:
    details_block = soup.find('div', attrs={'id':'titleDetails'})
    details = details_block.find_all('div', class_='txt-block')
    countries = []
    languages = []
    budget = 0
    box_office = 0

    # block is txt-block
    for b in details:
        if b.find('h4', class_='inline') is not None:   
            if b.find('h4', class_='inline').text == 'Country:':
                [countries.append(a.text) for a in b.find_all('a')]
            elif b.find('h4', class_='inline').text == 'Language:':
                [languages.append(a.text) for a in b.find_all('a')]
            elif b.find('h4', class_='inline').text == 'Budget:':
                budget = b.text
                budget = int(_clean_budget_string(budget))
            elif b.find('h4', class_='inline').text == 'Cumulative Worldwide Gross:':
                box_office = b.text
                box_office = int(_clean_box_office_string(box_office))

    summary = soup.find('div', class_='summary_text').text
    summary = _clean_summary_string(summary)

    movie_extra = models.MovieExtra(budget,box_office,countries,languages,summary)
    return movie_extra


def _scrap_movie_genres(soup:BeautifulSoup)->Dict:
    genres_c = soup.find('div',class_='subtext')
    genres_c = genres_c.find_all('a')
    genres = []
    for a in genres_c:
        if 'genres' in a['href']:
            genres.append(a.text)

    return genres


def _scrap_writers_directors(soup:BeautifulSoup):
    containers = soup.find_all('div', class_='credit_summary_item')
    writers = {}
    directors = {}
    for c in containers:
        if 'Director' in c.find('h4', class_='inline').text:
            for a in c.find_all('a'):
                directors['https://www.imdb.com/' + a['href']] = a.text
        elif 'Writer' in c.find('h4', class_='inline').text:
            for a in c.find_all('a'):
                if not 'fullcredits' in a['href']:
                    writers['https://www.imdb.com/' + a['href']] = a.text

    return writers, directors


def _scarp_cast(soup:BeautifulSoup)->List[List[str]]:
    """
    Scraps a cast from credits page
    Arguments:
        soup: BeutifulSoup
    Returns:
        cast: List[List[actor:str, url:str, character:str]]
    """
    table_container = soup.find('table', class_='cast_list')
    rows = table_container.find_all('tr')

    cast = []
    for row in rows[1::]:
        if row.find('td', class_='castlist_label'):
            break

        columns = row.find_all('td')
        actor = columns[1].text[2:-2]
        actor_url = urls.imdb + columns[1].find('a')['href']
        character =  columns[3].text.split('\n')[1]
        cast.append([actor, actor_url, character])

    return cast



def scrap_movie_page(url:str):
    """
    Scrap the movie page
    Arguments:
        url: str
    Returns:
        movie_extra: MovieExtra
        genres:      List[str]
        writers:     Dict{url:name}
        directors:   Dict{url:name}
        cast:        List[[name:str, url:str, character:str]]
    """
    soup = scrap_page(url)

    movie_extra = _scrap_movie_extra_parameters(soup)
    genres = _scrap_movie_genres(soup)
    writers, directors = _scrap_writers_directors(soup)
    print(writers)
    print(directors)

    cast_url = url + 'fullcredits'
    soup = scrap_page(cast_url)
    cast = _scarp_cast(soup)
    
    return movie_extra, genres, writers, directors, cast

